package com.rbkkl.judy.image;

import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobContainerClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@Disabled
class AzureImageRepositoryTest {

    @Mock
    private BlobContainerClient blobContainerClient;
    @InjectMocks
    private AzureImageRepository azureImageRepository;



    @Test
    void wrongConnectionString() {
        assertThrows(IllegalArgumentException.class, () -> new AzureImageRepository("123"));
    }


    @Test
    void uploadNonImage() {
        MultipartFile image = new MockMultipartFile(
                "file",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );

        FailedImageUploadException thrown = assertThrows(FailedImageUploadException.class, () -> azureImageRepository.uploadImage(image));
        assertTrue(thrown.getMessage().contains("mimetype"));
    }

    @Test
    void uploadImage() {
        MultipartFile image = new MockMultipartFile(
                "file",
                "hello.jpg",
                MediaType.IMAGE_JPEG_VALUE,
                "Hello, World!".getBytes()
        );

        var client = mock(BlobClient.class);
        when(blobContainerClient.getBlobClient(anyString())).thenReturn(client);
        var url = "https://judy.com";
        when(client.getBlobUrl()).thenReturn(url);
        azureImageRepository.uploadImage(image);
        verify(client).upload(any());
    }


    @Test
    void uploadImageFailed() {
        MultipartFile image = new MockMultipartFile(
                "file",
                "hello.jpg",
                MediaType.IMAGE_JPEG_VALUE,
                "Hello, World!".getBytes()
        );

        var client = mock(BlobClient.class);
        when(blobContainerClient.getBlobClient(anyString())).thenReturn(client);
        doThrow(RuntimeException.class).when(client).upload(any());
        assertThrows(FailedImageUploadException.class, () -> azureImageRepository.uploadImage(image));
    }
}