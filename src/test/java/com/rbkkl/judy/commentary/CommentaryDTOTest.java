package com.rbkkl.judy.commentary;

import com.rbkkl.judy.user.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CommentaryDTOTest {
    @Test
    void createCommentaryDTO() {
        User user = new User();
        Commentary commentary = new Commentary();
        CommentaryDTO commentaryDTO = new CommentaryDTO(user, commentary);

        assertEquals(user.getUsername(), commentaryDTO.getUsername());
        assertEquals(user.getAvatarURL(), commentaryDTO.getAvatarURL());
        assertEquals(commentary.getText(), commentaryDTO.getText());
        assertEquals(commentary.getTime(), commentaryDTO.getTime());
    }
}