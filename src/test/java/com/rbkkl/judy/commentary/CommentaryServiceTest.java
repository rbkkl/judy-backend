package com.rbkkl.judy.commentary;

import com.rbkkl.judy.post.Post;
import com.rbkkl.judy.sequence.SequenceService;
import com.rbkkl.judy.user.User;
import com.rbkkl.judy.user.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CommentaryServiceTest {
    @Mock
    private CommentaryRepository commentaryRepository;
    @Mock
    private UserService userService;
    @Mock
    private SequenceService sequenceService;
    @InjectMocks
    private CommentaryService commentaryService;

    @Test
    void add() {
        when(commentaryRepository.save(any(Commentary.class)))
                .thenAnswer(i -> i.getArguments()[0]);

        String userId = "userId";
        String postId = "postId";
        String text = "text";
        Instant instant = Instant.now();
        Commentary commentary = commentaryService.add(userId, postId, text, instant);

        assertEquals(userId, commentary.getUserId());
        assertEquals(postId, commentary.getPostId());
        assertEquals(text, commentary.getText());
        assertEquals(instant, commentary.getTime());
    }

    @Test
    void getAllCommentariesByUsername() {
        String userId = "userId";
        String username = "username";
        User user = new User();
        user.setId(userId);
        user.setUsername(username);
        var commentary = new Commentary();
        var commentary1 = new Commentary();
        var commentary2 = new Commentary();
        String commentaryId = "0";
        String commentaryId1 = "1";
        String commentaryId2 = "2";
        commentary.setId("0");
        commentary1.setId("1");
        commentary2.setId("2");
        List<String> commentaryIds = List.of(commentaryId, commentaryId1, commentaryId2);
        List<Commentary> commentaryList = List.of(commentary, commentary1, commentary2);
        user.setCommentariesIds(commentaryIds);

        when(userService.getByUsername(user, username)).thenReturn(Optional.of(user));
        when(commentaryRepository.findAllById(user.getCommentariesIds())).thenReturn(commentaryList);
        List<CommentaryWithPostIdDTO> commentaryWithPostIdDTOList = commentaryService.getAllCommentariesByUsername(user, username);

        assertThat(commentaryWithPostIdDTOList, hasSize(commentaryList.size()));
        assertThat(commentaryWithPostIdDTOList, everyItem(hasProperty("username", is(username))));
    }

    @Test
    void getCommentariesDTO() {
        String userId = "userId";
        String username = "username";
        var user = new User();
        user.setId(userId);
        user.setUsername(username);

        var post = new Post();
        var commentary = new Commentary();
        var commentary1 = new Commentary();
        var commentary2 = new Commentary();
        String commentaryId = "0";
        String commentaryText = "laalla1";
        String commentaryId1 = "1";
        String commentaryText1 = "laalla2";
        String commentaryId2 = "2";
        String commentaryText2 = "laalla3";
        commentary.setId(commentaryId);
        commentary.setText(commentaryText);
        commentary.setUserId(userId);
        commentary1.setId(commentaryId1);
        commentary1.setText(commentaryText1);
        commentary1.setUserId(userId);
        commentary2.setId(commentaryId2);
        commentary2.setText(commentaryText2);
        commentary2.setUserId(userId);
        List<String> commentaryIds = List.of(commentaryId, commentaryId1, commentaryId2);
        List<Commentary> commentaryList = List.of(commentary, commentary1, commentary2);
        post.setCommentariesIds(commentaryIds);

        when(commentaryRepository.findAllById(post.getCommentariesIds())).thenReturn(commentaryList);
        when(userService.getById(user, userId)).thenReturn(Optional.of(user));

        List<CommentaryDTO> dtos = commentaryService.getCommentariesDTO(user, post);

        assertThat(dtos, contains(
                hasProperty("text", is(commentaryText)),
                hasProperty("text", is(commentaryText1)),
                hasProperty("text", is(commentaryText2))
        ));
    }
}