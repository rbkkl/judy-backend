package com.rbkkl.judy.commentary;

import com.rbkkl.judy.user.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CommentaryWithPostIdDTOTest {
    @Test
    void createCommentaryWithIdDTO() {
        User user = new User();
        Commentary commentary = new Commentary();
        CommentaryWithPostIdDTO commentaryWithIdDTO = new CommentaryWithPostIdDTO(user, commentary);

        assertEquals(user.getUsername(), commentaryWithIdDTO.getUsername());
        assertEquals(user.getAvatarURL(), commentaryWithIdDTO.getAvatarURL());
        assertEquals(commentary.getText(), commentaryWithIdDTO.getText());
        assertEquals(commentary.getTime(), commentaryWithIdDTO.getTime());
        assertEquals(commentary.getPostId(), commentaryWithIdDTO.getPostId());
    }
}
