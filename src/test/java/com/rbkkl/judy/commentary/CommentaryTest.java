package com.rbkkl.judy.commentary;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNull;

class CommentaryTest {
    @Test
    void createCommentaryWithNoArgs() {
        var post = new Commentary();
        assertNull(post.getId());
        assertNull(post.getUserId());
        assertNull(post.getPostId());
        assertNull(post.getText());
        assertNull(post.getTime());
    }
}