package com.rbkkl.judy.commentary;

import com.rbkkl.judy.MockUserUtils;
import com.rbkkl.judy.user.User;
import com.rbkkl.judy.user.UserPrincipal;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.Instant;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
class CommentaryControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CommentaryService commentaryService;

    private static final UserPrincipal userPrincipal = MockUserUtils.geUserPrincipal("1", "user");
    private static final User user = userPrincipal.getUser();

    @Test
    void getUserCommentaries() throws Exception {
        String postId = "postId";
        String text = "text";
        Commentary commentary = new Commentary(postId, user.getId(), "postId", text, Instant.now());
        CommentaryWithPostIdDTO commentaryWithPostIdDTO = new CommentaryWithPostIdDTO(user, commentary);
        List<CommentaryWithPostIdDTO> list = List.of(commentaryWithPostIdDTO);
        when(commentaryService.getAllCommentariesByUsername(any(), any())).thenReturn(list);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/commentary")
                        .queryParam("username", userPrincipal.getUsername())
                        .with(user(userPrincipal))
                        .with(csrf()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].username").value(user.getUsername()))
                .andExpect(jsonPath("$[0].postId").value(postId))
                .andExpect(jsonPath("$[0].text").value(text));
    }
}