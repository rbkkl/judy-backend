package com.rbkkl.judy.util;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class DatabaseServiceTest {
    @Mock
    MongoTemplate mongoTemplate;
    @InjectMocks
    DatabaseService databaseService;


    @Test
    void modifySet() {
        var collection = "coll";
        databaseService.modifySet(collection, "123", "231", new Object(), DatabaseService.Action.ADD);
        verify(mongoTemplate).updateFirst(any(), any(), eq(collection));
    }
}