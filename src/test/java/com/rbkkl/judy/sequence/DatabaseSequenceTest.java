package com.rbkkl.judy.sequence;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DatabaseSequenceTest {
    @Test
    void databaseSequence() {
        String id = "1";
        long seq = 111;
        var ds = new DatabaseSequence(id, seq);
        assertEquals(id, ds.getId());
        assertEquals(seq, ds.getSequence());
    }

}