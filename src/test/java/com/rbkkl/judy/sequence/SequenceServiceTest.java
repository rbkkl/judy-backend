package com.rbkkl.judy.sequence;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.mongodb.core.MongoOperations;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class SequenceServiceTest {
    @Mock
    private MongoOperations mongoOperations;
    @InjectMocks
    private SequenceService sequenceService;

    @Test
    void generateNextSequence() {
        String sequenceName = "SEQUENCE";
        assertEquals(1, sequenceService.generateNextSequence(sequenceName));
    }
}