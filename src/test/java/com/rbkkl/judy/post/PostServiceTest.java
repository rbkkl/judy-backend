package com.rbkkl.judy.post;

import com.rbkkl.judy.commentary.Commentary;
import com.rbkkl.judy.commentary.CommentaryDTO;
import com.rbkkl.judy.commentary.CommentaryService;
import com.rbkkl.judy.image.AzureImageRepository;
import com.rbkkl.judy.user.User;
import com.rbkkl.judy.user.UserRepository;
import com.rbkkl.judy.user.UserService;
import com.rbkkl.judy.util.DatabaseService;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class PostServiceTest {
    @Mock
    private CommentaryService commentaryService;
    @Mock
    private PostRepository postRepository;
    @Mock
    private PostIdGenerator generatorUUID;
    @Mock
    private AzureImageRepository imageRepository;
    @Mock
    private UserRepository userRepository;
    @Mock
    private UserService userService;
    @Mock
    private DatabaseService databaseService;
    @InjectMocks
    private PostService postService;
    private User user;
    private User user1;
    private User user2;
    private Post post;
    private Post post1;
    private Post post2;
    private final String userId = "11234";
    private final String userId1 = "211234";
    private final String userId2 = "311234";
    private final String username = "[pewroiu";
    private final String username1 = "mnbv";
    private final String username2 = "[pjkn32";
    private final String postId = "411234";
    private final String postId1 = "5211234";
    private final String postId2 = "6311234";

    private static final String COLLECTION_NAME = "posts";
    private static final String DISLIKED_SET_NAME = "dislikedIds";
    private static final String LIKED_SET_NAME = "likedIds";

    @BeforeEach
    void setUser() {
        user = new User();
        user1 = new User();
        user2 = new User();
        user.setId(userId);
        user1.setId(userId1);
        user2.setId(userId2);
        user.setUsername(username);
        user1.setUsername(username1);
        user2.setUsername(username2);
        post = new Post();
        post1 = new Post();
        post2 = new Post();
        post.setId(postId);
        post1.setId(postId1);
        post2.setId(postId2);
        post.setAuthorId(userId);
        post1.setAuthorId(userId1);
        post2.setAuthorId(userId2);
        user.setPostsIds(new ArrayList<>(List.of(postId)));
        user1.setPostsIds(new ArrayList<>(List.of(postId1)));
        user2.setPostsIds(new ArrayList<>(List.of(postId2)));
    }

    @SneakyThrows
    @Test
    void publishPost() {
        MultipartFile[] images = new MultipartFile[1];
        images[0] = new MockMultipartFile("file", new byte[0]);
        String uuid = "uuid";
        String text = "text";
        URL url = new URL("https://judy.rbkkl.tech/login");
        when(imageRepository.uploadImage(any())).thenReturn(url);
        when(generatorUUID.generate(any(), any())).thenReturn(uuid);
        when(postRepository.save(any(Post.class)))
                .thenAnswer(i -> i.getArguments()[0]);
        when(userRepository.findById(userId)).thenReturn(Optional.ofNullable(user));
        PostDTO postDTO = postService.publishPost(user, text, images);

        assertEquals(postDTO.getId(), uuid);
        assertEquals(postDTO.getText(), text);
        assertThat(postDTO.getImageURLS(), hasItem(url));
    }

    @Test
    void getAllPostsSize() {
        List<Post> posts = List.of(post, post1, post2);

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(userRepository.findById(userId1)).thenReturn(Optional.of(user1));
        when(userRepository.findById(userId2)).thenReturn(Optional.of(user2));
        when(userService.getAll(user)).thenReturn(List.of(user, user1, user2));
        when(postRepository.findAllById(List.of(postId))).thenReturn(List.of(post));
        when(postRepository.findAllById(List.of(postId1))).thenReturn(List.of(post1));
        when(postRepository.findAllById(List.of(postId2))).thenReturn(List.of(post2));

        assertEquals(3, postService.getAllPosts(user).size());
    }

    @Test
    void getAllPosts() {
        List<Post> posts = List.of(post, post1, post2);
        List<PostDTO> postsDTO = List.of(new PostDTO(post, List.of(), user, PostReaction.NONE),
                new PostDTO(post1, List.of(), user1, PostReaction.NONE),
                new PostDTO(post2, List.of(), user2, PostReaction.NONE));

        when(userService.getAll(user)).thenReturn(List.of(user, user1, user2));
//        when(userRepository.findAll()).thenReturn(List.of(user, user1, user2));
        when(postRepository.findAllById(List.of(postId))).thenReturn(List.of(post));
        when(postRepository.findAllById(List.of(postId1))).thenReturn(List.of(post1));
        when(postRepository.findAllById(List.of(postId2))).thenReturn(List.of(post2));
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(userRepository.findById(userId1)).thenReturn(Optional.of(user1));
        when(userRepository.findById(userId2)).thenReturn(Optional.of(user2));

        assertEquals(postsDTO, postService.getAllPosts(user));
    }

    @Test
    void getPost() {
        var postDTO = new PostDTO(post, List.of(), user, PostReaction.NONE);

        when(postRepository.findById(postId)).thenReturn(Optional.of(post));
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(userService.isUserShowable(any(), any())).thenReturn(true);

        assertEquals(Optional.of(postDTO), postService.getPost(user, postId));
    }

    @Test
    void getUserPosts() {
        Post userPost1 = new Post();
        Post userPost2 = new Post();
        String userPost1Id = "jigadfg";
        String userPost2Id = "gjigadfg";
        userPost1.setId(userPost1Id);
        userPost2.setId(userPost2Id);
        userPost1.setAuthorId(userId);
        userPost2.setAuthorId(userId);

        user.setPostsIds(List.of(postId, userPost1Id, userPost2Id));

        List<PostDTO> postsDTO = List.of(new PostDTO(post, List.of(), user, PostReaction.NONE),
                new PostDTO(userPost1, List.of(), user, PostReaction.NONE),
                new PostDTO(userPost2, List.of(), user, PostReaction.NONE));

        when(userService.getByUsername(user, username)).thenReturn(Optional.of(user));
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(postRepository.findById(postId)).thenReturn(Optional.of(post));
        when(postRepository.findById(userPost1Id)).thenReturn(Optional.of(userPost1));
        when(postRepository.findById(userPost2Id)).thenReturn(Optional.of(userPost2));

        assertEquals(postsDTO, postService.getUserPosts(user, username));
    }

    @Test
    void compareByCreationDateDesc() {
        var postDtoA = mock(PostDTO.class);
        var postDtoB = mock(PostDTO.class);
        doReturn(Instant.now()).when(postDtoA).getTime();
        doReturn(Instant.now()).when(postDtoB).getTime();
        assertEquals(0, PostService.compareByCreationDateDesc(postDtoA, postDtoA));
        assertTrue(PostService.compareByCreationDateDesc(postDtoA, postDtoB) > 0);
        assertTrue(PostService.compareByCreationDateDesc(postDtoB, postDtoA) < 0);
    }

    @Test
    void getFollowingUsersPosts() {
        user.setFollowingIds(List.of(userId1, userId2));
        user1.setFollowerIds(List.of(userId));
        user2.setFollowerIds(List.of(userId));
        List<PostDTO> postsDTO = List.of(new PostDTO(post1, List.of(), user1, PostReaction.NONE),
                new PostDTO(post2, List.of(), user2, PostReaction.NONE));

        when(userService.getById(user, userId1)).thenReturn(Optional.ofNullable(user1));
        when(userService.getById(user, userId2)).thenReturn(Optional.ofNullable(user2));
        when(userRepository.findById(userId1)).thenReturn(Optional.of(user1));
        when(userRepository.findById(userId2)).thenReturn(Optional.of(user2));
        when(postRepository.findAllById(List.of(postId1))).thenReturn(List.of(post1));
        when(postRepository.findAllById(List.of(postId2))).thenReturn(List.of(post2));
        assertEquals(postsDTO, postService.getFollowingUsersPosts(user));
    }

    @Test
    void processReactionNone() {
        postService.processReaction(userId, postId, PostReaction.NONE);
        verify(databaseService).modifySet(COLLECTION_NAME, postId, DISLIKED_SET_NAME, userId, DatabaseService.Action.REMOVE);
        verify(databaseService).modifySet(COLLECTION_NAME, postId, LIKED_SET_NAME, userId, DatabaseService.Action.REMOVE);
    }


    @Test
    void processReactionLike() {
        postService.processReaction(userId, postId, PostReaction.LIKE);
        verify(databaseService).modifySet(COLLECTION_NAME, postId, DISLIKED_SET_NAME, userId, DatabaseService.Action.REMOVE);
        verify(databaseService).modifySet(COLLECTION_NAME, postId, LIKED_SET_NAME, userId, DatabaseService.Action.ADD);
    }


    @Test
    void processReactionDislike() {
        postService.processReaction(userId, postId, PostReaction.DISLIKE);
        verify(databaseService).modifySet(COLLECTION_NAME, postId, DISLIKED_SET_NAME, userId, DatabaseService.Action.ADD);
        verify(databaseService).modifySet(COLLECTION_NAME, postId, LIKED_SET_NAME, userId, DatabaseService.Action.REMOVE);
    }

    @Test
    void addCommentary() {
        String commentaryText = "commentary text";
        String commentaryId = "afadrfg";
        var commentaryDTO = new CommentaryDTO(username1, null, commentaryText, null);

        Instant time = Instant.now();
        Commentary commentary = new Commentary(commentaryId, userId1, postId, commentaryText, time);

        when(postRepository.findById(postId)).thenReturn(Optional.of(post));
        when(commentaryService.add(any(), any(), any(), any())).thenReturn(commentary);
        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        when(commentaryService.getCommentariesDTO(user1, post)).thenReturn(List.of(commentaryDTO));
        Optional<PostDTO> optionalPostDTO = postService.addCommentary(user1, postId, commentaryText);

        assertTrue(optionalPostDTO.isPresent());
        assertThat(optionalPostDTO.get().getCommentaries(), hasItem(hasProperty("text", is(commentaryText))));
        assertThat(optionalPostDTO.get().getCommentaries(), hasItem(hasProperty("username", is(username1))));
    }
}