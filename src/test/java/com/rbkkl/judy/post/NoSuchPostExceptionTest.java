package com.rbkkl.judy.post;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

class NoSuchPostExceptionTest {
    @Test
    void exceptionMessage() {
        var id = "123";
        var e = new NoSuchPostException(id);

        assertThat(e.getMessage(), containsString(id));
    }
}