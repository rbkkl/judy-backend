package com.rbkkl.judy.post;

import com.rbkkl.judy.MockUserUtils;
import com.rbkkl.judy.user.NoSuchUserException;
import com.rbkkl.judy.user.User;
import com.rbkkl.judy.user.UserPrincipal;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
class PostControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PostService postService;

    private static final UserPrincipal userPrincipal = MockUserUtils.geUserPrincipal("1", "user");
    private static final User user = userPrincipal.getUser();

    @Test
    void publishPost() throws Exception {
        var file = new MockMultipartFile("images[]", "orig", null, new byte[0]);
        var files = new MockMultipartFile[1];
        files[0] = file;
        String text = "text";

        mockMvc.perform(multipart("/post")
                        .file(file).file(file)
                        .param("text", text)
                        .with(user(userPrincipal))
                        .with(csrf()))
                .andExpect(status().isOk());
    }

    @Test
    void getPost200() throws Exception {
        String postId = "1";
        var postDTO = new PostDTO(new Post(), List.of(), user, PostReaction.NONE);
        when(postService.getPost(user, postId)).thenReturn(Optional.of(postDTO));
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/post/" + postId)
                        .with(user(userPrincipal))
                        .with(csrf()))
                .andExpect(status().isOk());
    }

    @Test
    void getPost404() throws Exception {
        String postId = "1";
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/post/" + postId)
                        .with(user(userPrincipal))
                        .with(csrf()))
                .andExpect(status().is(404));
    }

    @Test
    void getPosts200() throws Exception {
        String username = "username";
        List<PostDTO> list = List.of(new PostDTO(new Post(), List.of(), user, PostReaction.NONE));
        when(postService.getUserPosts(user, username)).thenReturn(list);
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/post/")
                        .with(user(userPrincipal))
                        .queryParam("user", username)
                        .with(csrf()))
                .andExpect(status().isOk());
    }

    @Test
    void getPosts404() throws Exception {
        String username = "123";
        when(postService.getUserPosts(user, username)).thenThrow(new NoSuchUserException(username));
        mockMvc.perform(MockMvcRequestBuilders
                        .get("/post/")
                        .with(user(userPrincipal))
                        .queryParam("user", username)
                        .with(csrf()))
                .andExpect(status().is(404));
    }
}