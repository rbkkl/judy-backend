package com.rbkkl.judy.post;

import com.rbkkl.judy.commentary.CommentaryDTO;
import com.rbkkl.judy.user.User;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PostDTOTest {
    @Test
    void createPostDTOFromPost() {
        Post post = new Post();
        List<CommentaryDTO > commentariesDTO = List.of();
        User user = new User();
        PostReaction reaction = PostReaction.DISLIKE;
        var postDto = new PostDTO(post, commentariesDTO, user, reaction);

        assertEquals(post.getId(), postDto.getId());
        assertEquals(post.getText(), postDto.getText());
        assertEquals(post.getImageURLs(), postDto.getImageURLS());
        assertEquals(post.getTime(), postDto.getTime());
        assertEquals(post.getLikedIds().size(), postDto.getLikesAmount());
        assertEquals(post.getDislikedIds().size(), postDto.getDislikesAmount());
        assertEquals(commentariesDTO, postDto.getCommentaries());
        assertEquals(user.getUsername(), postDto.getUsername());
        assertEquals(user.getAvatarURL(), postDto.getAvatarURL());
        assertEquals(reaction, postDto.getReaction());
    }
}