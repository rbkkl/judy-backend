package com.rbkkl.judy.post;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PostTest {
    @Test
    void createPostWithNoArgs() {
        var post = new Post();
        assertNull(post.getId());
        assertNull(post.getAuthorId());
        assertNull(post.getText());
        assertNull(post.getImageURLs());
        assertNull(post.getTime());
        assertNotNull(post.getCommentariesIds());
        assertEquals(List.of(), post.getCommentariesIds());
        assertNotNull(post.getLikedIds());
        assertEquals(List.of(), post.getLikedIds());
        assertNotNull(post.getDislikedIds());
        assertEquals(List.of(), post.getDislikedIds());
    }


    @Test
    void addCommentary() {
        var post = new Post();
        var id = "123";
        post.addCommentary(id);
        assertEquals(List.of(id), post.getCommentariesIds());
    }
}