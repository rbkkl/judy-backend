package com.rbkkl.judy.post;

import com.rbkkl.judy.sequence.SequenceService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
class PostIdGeneratorTest {
    @Mock
    private SequenceService sequenceService;
    @InjectMocks
    private PostIdGenerator postIdGenerator;

    @Test
    void generate() {
        var id = "1000000000000000000000000000";
        var time = LocalDateTime.of(2022, 1, 1, 12, 0);
        assertEquals("AAFJlwAAAAA=", postIdGenerator.generate(id, time));
    }
}