package com.rbkkl.judy.user;

import com.rbkkl.judy.MockUserUtils;
import com.rbkkl.judy.image.AzureImageRepository;
import com.rbkkl.judy.util.DatabaseService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private AzureImageRepository imageRepository;
    @Mock
    private DatabaseService databaseService;
    @InjectMocks
    private UserService userService;

    private final String userId = "userId";
    private final String user1Id = "1";
    private final String user2Id = "2";
    private final String user3Id = "3";
    private final String username = "username";
    private final String username1 = "username1";
    private final String username2 = "username2";
    private final String username3 = "username3";
    private final List<String> followerIds = List.of(user1Id, user2Id, user3Id);

    private User regularActiveUser;
    private User user1;
    private User user2;
    private User user3;
    private List<User> users;
    private User admin;
    private User blockedUser;

    @BeforeEach
    void setUser() {
        regularActiveUser = new User();
        regularActiveUser.setId(userId);
        regularActiveUser.setUsername(username);
        regularActiveUser.setFollowerIds(followerIds);
        regularActiveUser.setStatus(UserStatus.ACTIVE);
        regularActiveUser.setRole(UserRole.REGULAR);
        user1 = new User();
        user1.setId(user1Id);
        user1.setUsername(username1);
        user2 = new User();
        user2.setId(user2Id);
        user2.setUsername(username2);
        user3 = new User();
        user3.setId(user3Id);
        user3.setUsername(username3);
        users = List.of(user1, user2, user3);
        admin = new User();
        admin.setId("123");
        admin.setUsername("admin");
        admin.setRole(UserRole.ADMIN);
        blockedUser = new User();
        blockedUser.setId("321");
        blockedUser.setUsername("blocked");
        blockedUser.setStatus(UserStatus.BLOCKED);
    }

    @Test
    void getPublicInfoByUsername() {
        when(userRepository.findByUsername(username)).thenReturn(Optional.of(regularActiveUser));
        UserPublicInfoDTO userPublicInfoDTO = userService.getPublicInfoByUsername(regularActiveUser, username);

        assertEquals(username, userPublicInfoDTO.getUsername());
    }

    @Test
    void setUserInfo() {
        String newUsername = "new_username";
        userService.setUserInfo(regularActiveUser, newUsername, Optional.empty());

        verify(userRepository).save(regularActiveUser);
        assertEquals(newUsername, regularActiveUser.getUsername());
    }

    @Test
    void setIncorrectUserInfo() {
        String incorrectUsername = "INCORRECT123";

        IncorrectNameException thrown = assertThrows(IncorrectNameException.class, () -> userService.setUserInfo(regularActiveUser, incorrectUsername, Optional.empty()));

        assertTrue(thrown.getMessage().contains(incorrectUsername));
    }

    @Test
    void getUserByUsername() {
        when(userRepository.findByUsername(username)).thenReturn(Optional.of(regularActiveUser));
        Optional<User> optionalUser = userService.getByUsername(regularActiveUser, username);

        assertTrue(optionalUser.isPresent());
        assertEquals(username, optionalUser.get().getUsername());
    }

    @Test
    void getUserFollowersByUsername() {
        when(userRepository.findByUsername(username)).thenReturn(Optional.of(regularActiveUser));
        when(userRepository.findById(user1Id)).thenReturn(Optional.of(user1));
        when(userRepository.findById(user2Id)).thenReturn(Optional.of(user2));
        when(userRepository.findById(user3Id)).thenReturn(Optional.of(user3));

        List<FollowerDTO> followerDTOList = userService.getUserFollowersByUsername(regularActiveUser, username);
        assertThat(followerDTOList, contains(hasProperty("username", is(username1)), hasProperty("username", is(username2)), hasProperty("username", is(username3))));
    }

    @Test
    void banUserByAdmin() {
        when(userRepository.findByUsername(regularActiveUser.getUsername())).thenReturn(Optional.ofNullable(regularActiveUser));
        userService.banUser(admin, regularActiveUser.getUsername());
        ArgumentCaptor<User> bannedUser = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(bannedUser.capture());
        assertTrue(bannedUser.getValue().isBlocked());
    }

    @Test
    void banUserByNotAdmin() {
        NotEnoughRightsException thrown = assertThrows(NotEnoughRightsException.class, () -> userService.banUser(regularActiveUser, regularActiveUser.getUsername()));

        assertTrue(thrown.getMessage().contains(regularActiveUser.getUsername()));
    }

    @Test
    void unbanUserByAdmin() {
        when(userRepository.findByUsername(blockedUser.getUsername())).thenReturn(Optional.ofNullable(blockedUser));
        userService.unbanUser(admin, blockedUser.getUsername());
        ArgumentCaptor<User> unbannedUser = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(unbannedUser.capture());
        assertFalse(unbannedUser.getValue().isBlocked());
    }

    @Test
    void unbanUserByNotAdmin() {
        NotEnoughRightsException thrown = assertThrows(NotEnoughRightsException.class, () -> userService.unbanUser(regularActiveUser, blockedUser.getUsername()));

        assertTrue(thrown.getMessage().contains(regularActiveUser.getUsername()));
    }

    @Test
    void isUserShowableAdmin() {
        assertTrue(userService.isUserShowable(admin, blockedUser));
    }

    @Test
    void isUserShowableSameUser() {
        assertTrue(userService.isUserShowable(blockedUser, blockedUser));
    }

    @Test
    void processFollowIncorrectNickname() {
        var nickname = "nonickname";
        IncorrectNameException thrown = assertThrows(IncorrectNameException.class, () -> userService.processFollow(regularActiveUser, nickname, UserService.Action.FOLLOW));

        assertTrue(thrown.getMessage().contains(nickname));
    }

    @Test
    void processFollow() {
        var toFollow = MockUserUtils.getUser();
        when(userRepository.findByUsername(toFollow.getUsername())).thenReturn(Optional.of(toFollow));
        userService.processFollow(regularActiveUser, toFollow.getUsername(), UserService.Action.FOLLOW);
        verify(databaseService).modifySet("users", toFollow.getId(), "followerIds", regularActiveUser.getId(), DatabaseService.Action.ADD);
        verify(databaseService).modifySet("users", regularActiveUser.getId(), "followingIds", toFollow.getId(), DatabaseService.Action.ADD);
    }

    @Test
    void filterBannedIdsAdmin() {
        lenient().when(userRepository.findByStatus(UserStatus.BLOCKED)).thenReturn(users);

        assertEquals(followerIds, userService.filterBannedIds(admin, followerIds));
    }
}