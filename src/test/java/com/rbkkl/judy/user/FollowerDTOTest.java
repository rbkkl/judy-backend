package com.rbkkl.judy.user;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FollowerDTOTest {
    @Test
    void createFollowerDTO() {
        User user = new User();
        FollowerDTO followerDTO = new FollowerDTO(user);

        assertEquals(user.getUsername(), followerDTO.getUsername());
        assertEquals(user.getAvatarURL(), followerDTO.getAvatarURL());
    }
}