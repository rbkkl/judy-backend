package com.rbkkl.judy.user;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

class IncorrectNameExceptionTest {
    @Test
    void exceptionMessage() {
        var username = "123";
        var e = new IncorrectNameException(username);

        assertThat(e.getMessage(), containsString(username));
    }
}