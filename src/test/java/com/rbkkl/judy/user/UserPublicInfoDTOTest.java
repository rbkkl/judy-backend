package com.rbkkl.judy.user;

import com.rbkkl.judy.MockUserUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class UserPublicInfoDTOTest {
    @Test
    void createUserPublicInfoDTO() {
        var user = MockUserUtils.getUser();
        UserPublicInfoDTO userPublicInfoDTO = new UserPublicInfoDTO(user, false);

        assertEquals(user.getUsername(), userPublicInfoDTO.getUsername());
        assertEquals(user.getAvatarURL(), userPublicInfoDTO.getAvatarURL());
        assertEquals(user.getFollowerIds().size(), userPublicInfoDTO.getFollowersAmount());
        assertEquals(user.getCommentariesIds().size(), userPublicInfoDTO.getCommentariesAmount());
        assertEquals(user.getStatus(), userPublicInfoDTO.getStatus());
        assertFalse(userPublicInfoDTO.isSubscribed());
    }
}