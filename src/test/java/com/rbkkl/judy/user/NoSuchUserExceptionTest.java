package com.rbkkl.judy.user;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

class NoSuchUserExceptionTest {
    @Test
    void exceptionMessage() {
        var username = "123";
        var e = new NoSuchUserException(username);

        assertThat(e.getMessage(), containsString(username));
    }
}