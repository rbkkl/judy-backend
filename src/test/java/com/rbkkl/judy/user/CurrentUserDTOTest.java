package com.rbkkl.judy.user;

import com.rbkkl.judy.MockUserUtils;
import org.junit.jupiter.api.Test;

import java.net.MalformedURLException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertEquals;

class CurrentUserDTOTest {
    @Test
    void createCurrentUserDTO() throws MalformedURLException {
        var user = MockUserUtils.getUser();
        var dto = new CurrentUserDTO(user);
        assertThat(dto, not(hasProperty("id")));
        assertEquals(user.getUsername(), dto.getUsername());
        assertEquals(user.getAvatarURL(), dto.getAvatarURL());
        assertEquals(user.getRole(), dto.getRole());
        assertEquals(user.getStatus(), dto.getStatus());
    }

}