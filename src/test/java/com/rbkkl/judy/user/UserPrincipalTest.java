package com.rbkkl.judy.user;

import com.rbkkl.judy.MockUserUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserPrincipalTest {
    @Test
    void createUserPrincipal() {
        var user = MockUserUtils.getUser();
        var userPrincipal = new UserPrincipal(user);
        assertEquals(user, userPrincipal.getUser());
        assertEquals(user.getUsername(), userPrincipal.getUsername());
        assertTrue(userPrincipal.isAccountNonExpired());
        assertTrue(userPrincipal.isAccountNonLocked());
        assertTrue(userPrincipal.isEnabled());
        assertTrue(userPrincipal.isCredentialsNonExpired());
    }

}