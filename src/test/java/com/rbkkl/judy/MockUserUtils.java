package com.rbkkl.judy;

import com.rbkkl.judy.user.User;
import com.rbkkl.judy.user.UserPrincipal;
import com.rbkkl.judy.user.UserRole;
import com.rbkkl.judy.user.UserStatus;
import lombok.SneakyThrows;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MockUserUtils {
    public static UserPrincipal geUserPrincipal(String id, String username) {
        User user = new User();
        user.setId(id);
        user.setUsername(username);
        user.setStatus(UserStatus.ACTIVE);
        user.setRole(UserRole.REGULAR);

        return new UserPrincipal(user);
    }

    @SneakyThrows
    public static User getUser() {
        String id = "2131";
        String username = "aadlfas";
        URL avatarURL = new URL("https://judy.com");
        List<String> postsIds = new ArrayList<>(List.of("13", "124"));
        List<String> commentariesIds = new ArrayList<>(List.of("1231", "2132151"));
        List<String> followerIds = new ArrayList<>(List.of("21321321312"));
        List<String> followingIds = new ArrayList<>();
        UserRole role = UserRole.REGULAR;
        UserStatus status = UserStatus.ACTIVE;
        return new User(id, username, avatarURL, postsIds, commentariesIds, followerIds, followingIds, role, status);
    }
}