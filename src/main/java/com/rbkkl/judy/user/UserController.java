package com.rbkkl.judy.user;

import com.rbkkl.judy.image.FailedImageUploadException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @PatchMapping("/set-info")
    @ResponseBody
    public ResponseEntity<CurrentUserDTO> setUserInfo(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                                      @RequestParam(name = "username") String newUsername,
                                                      @RequestParam(name = "image") Optional<MultipartFile> newAvatar) {
        try {
            CurrentUserDTO userChanged = userService.setUserInfo(userPrincipal.getUser(), newUsername, newAvatar);
            return ResponseEntity.ok(userChanged);
        } catch (NoSuchUserException | IncorrectNameException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e.getMessage(), e);
        } catch (FailedImageUploadException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

    @GetMapping("/me")
    public ResponseEntity<CurrentUserDTO> getCurrentUser(@AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(userService.getCurrentUser(userPrincipal.getUser()));
    }

    @GetMapping("/{username}")
    public ResponseEntity<UserPublicInfoDTO> getPublicInfoByUsername(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                                                     @PathVariable("username") String username) {
        try {
            UserPublicInfoDTO user = userService.getPublicInfoByUsername(userPrincipal.getUser(), username);
            return ResponseEntity.ok(user);
        } catch (NoSuchUserException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @PatchMapping("/action")
    public ResponseEntity<UserPublicInfoDTO> action(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                                    @RequestParam(name = "name") String followingName,
                                                    @RequestParam(name = "act") String action) {
        if (userPrincipal.getUsername().equals(followingName)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Self-following is not allowed");
        }
        try {
            userService.processFollow(userPrincipal.getUser(), followingName, UserService.Action.valueOf(action));
            return getPublicInfoByUsername(userPrincipal, userPrincipal.getUsername());
        } catch (NoSuchUserException | IncorrectNameException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e.getMessage(), e);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad act parameter: " + action);
        }
    }

    @GetMapping("/{username}/followers")
    public ResponseEntity<List<FollowerDTO>> getFollowers(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                                          @PathVariable("username") String username) {
        try {
            return ResponseEntity.ok(userService.getUserFollowersByUsername(userPrincipal.getUser(), username));
        } catch (NoSuchUserException e) {
            // only if username from query is incorrect
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @PostMapping("/{username}/ban")
    public ResponseEntity<UserPublicInfoDTO> banUser(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                                     @PathVariable("username") String username) {
        try {
            UserPublicInfoDTO bannedUser = userService.banUser(userPrincipal.getUser(), username);
            return ResponseEntity.ok(bannedUser);
        } catch (NotEnoughRightsException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage(), e);
        } catch (NoSuchUserException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }

    @PostMapping("/{username}/unban")
    public ResponseEntity<UserPublicInfoDTO> unbanUser(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                                       @PathVariable("username") String username) {
        try {
            UserPublicInfoDTO unbannedUser = userService.unbanUser(userPrincipal.getUser(), username);
            return ResponseEntity.ok(unbannedUser);
        } catch (NotEnoughRightsException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage(), e);
        } catch (NoSuchUserException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }
}
