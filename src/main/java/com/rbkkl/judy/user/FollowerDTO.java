package com.rbkkl.judy.user;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.net.URL;

@Data
@AllArgsConstructor
public class FollowerDTO {
    private final String username;
    private final URL avatarURL;

    public FollowerDTO(User user) {
        this(user.getUsername(), user.getAvatarURL());
    }
}
