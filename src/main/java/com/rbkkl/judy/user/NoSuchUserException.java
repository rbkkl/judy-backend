package com.rbkkl.judy.user;

public class NoSuchUserException extends RuntimeException {
    public NoSuchUserException(String username) {
        super("No such user: " + username);
    }
}
