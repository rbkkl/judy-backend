package com.rbkkl.judy.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "users")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
        @Id
        private String id;
        @Indexed(unique = true)
        private String username;
        private URL avatarURL;
        private List<String> postsIds = new ArrayList<>();
        private List<String> commentariesIds = new ArrayList<>();
        private List<String> followerIds = new ArrayList<>();
        private List<String> followingIds = new ArrayList<>();
        private UserRole role;
        private UserStatus status;

        public void addPost(String postId) {
                postsIds.add(postId);
        }

        public void addCommentary(String commentaryId) {
                commentariesIds.add(commentaryId);
        }

        public boolean isAdmin() {
                return UserRole.ADMIN.equals(role);
        }

        public boolean isBlocked() {
                return UserStatus.BLOCKED.equals(status);
        }
}
