package com.rbkkl.judy.user;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.net.URL;

@Data
@AllArgsConstructor
public class UserPublicInfoDTO {
    private final String username;
    private final URL avatarURL;
    private final int followersAmount;
    private final int commentariesAmount;
    private final boolean isSubscribed;
    private final UserStatus status;

    public UserPublicInfoDTO(User user, boolean isSubscribed) {
        this(user.getUsername(),
                user.getAvatarURL(),
                user.getFollowerIds().size(),
                user.getCommentariesIds().size(),
                isSubscribed,
                user.getStatus());
    }
}