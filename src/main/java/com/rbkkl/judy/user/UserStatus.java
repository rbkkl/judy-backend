package com.rbkkl.judy.user;

public enum UserStatus {
    ACTIVE,
    BLOCKED
}
