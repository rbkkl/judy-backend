package com.rbkkl.judy.user;

public class NotEnoughRightsException extends RuntimeException {
    public NotEnoughRightsException(String username) {
        super("User " + username + " does NOT have enough rights!");
    }
}
