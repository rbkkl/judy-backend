package com.rbkkl.judy.user;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.net.URL;
import java.util.List;

@Data
@AllArgsConstructor
public class CurrentUserDTO {
    private final String username;
    private final URL avatarURL;
    private final UserStatus status;
    private final UserRole role;

    public CurrentUserDTO(User user) {
        this(user.getUsername(), user.getAvatarURL(), user.getStatus(), user.getRole());
    }
}