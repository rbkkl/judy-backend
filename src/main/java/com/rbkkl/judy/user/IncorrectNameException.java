package com.rbkkl.judy.user;

public class IncorrectNameException extends RuntimeException {
    public IncorrectNameException(String username) {
        super("Incorrect username: " + username);
    }
}