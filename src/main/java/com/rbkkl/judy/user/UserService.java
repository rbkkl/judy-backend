package com.rbkkl.judy.user;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.rbkkl.judy.image.AzureImageRepository;
import com.rbkkl.judy.image.FailedImageUploadException;
import com.rbkkl.judy.util.DatabaseService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {
    Logger logger = LoggerFactory.getLogger(UserService.class);
    private final UserRepository userRepository;
    private final AzureImageRepository imageRepository;
    private final DatabaseService databaseService;

    public UserPublicInfoDTO banUser(User currentUser, String username) throws NotEnoughRightsException, NoSuchUserException {
        if (currentUser.isAdmin()) {
            User userToBan = userRepository.findByUsername(username).orElseThrow(() -> new NoSuchUserException(username));
            userToBan.setStatus(UserStatus.BLOCKED);
            userRepository.save(userToBan);

            return createUserPublicInfoDTO(currentUser, userToBan);
        } else {
            throw new NotEnoughRightsException(currentUser.getUsername());
        }
    }

    public UserPublicInfoDTO unbanUser(User currentUser, String username) throws NotEnoughRightsException, NoSuchUserException {
        if (currentUser.isAdmin()) {
            User userToUnban = userRepository.findByUsername(username).orElseThrow(() -> new NoSuchUserException(username));
            userToUnban.setStatus(UserStatus.ACTIVE);
            userRepository.save(userToUnban);

            return createUserPublicInfoDTO(currentUser, userToUnban);
        } else {
            throw new NotEnoughRightsException(currentUser.getUsername());
        }
    }

    public enum Action {
        UNFOLLOW, FOLLOW
    }

    public User getByPayload(GoogleIdToken.Payload payload) {
        return userRepository.findById(payload.getSubject()).orElseGet(() -> createFromPayload(payload));
    }

    public UserPrincipal getPrincipal(User user) {
        return new UserPrincipal(user);
    }

    public UserPrincipal getPrincipal(GoogleIdToken.Payload payload) {
        return getPrincipal(getByPayload(payload));
    }

    public boolean isUserShowable(User currentUser, User user) {
        return !user.isBlocked() || currentUser.getUsername().equals(user.getUsername()) || currentUser.isAdmin();
    }

    public List<User> getAllBanned() {
        return userRepository.findByStatus(UserStatus.BLOCKED);
    }

    public Optional<User> getById(User currentUser, String userId) {
        return userRepository.findById(userId).stream().filter(user -> isUserShowable(currentUser, user)).findFirst();
    }

    public Optional<User> getByUsername(User currentUser, String username) {
        return userRepository.findByUsername(username).stream().filter(user -> isUserShowable(currentUser, user)).findFirst();
    }

    public List<User> getAll(User currentUser) {
        return userRepository.findAll().stream().filter(user -> isUserShowable(currentUser, user)).toList();
    }

    public UserPublicInfoDTO getPublicInfoByUsername(User currentUser, String username) throws NoSuchUserException {
        User user = getByUsername(currentUser, username).orElseThrow(() -> new NoSuchUserException(username));

        return createUserPublicInfoDTO(currentUser, user);
    }

    public CurrentUserDTO setUserInfo(User user, String newUsername, Optional<MultipartFile> newAvatar) throws NoSuchUserException {
        if (!correctUsername(newUsername) || isUsernameAlreadyTaken(newUsername, user.getUsername())) {
            throw new IncorrectNameException(newUsername);
        }

        user = updateUser(user, newUsername, newAvatar);
        return new CurrentUserDTO(user);
    }

    private boolean isUsernameAlreadyTaken(String newUsername, String oldUsername) {
        var possibleUser = userRepository.findByUsername(newUsername);
        return possibleUser.isPresent() && !newUsername.equals(oldUsername);
    }

    // actually URL constructor never throws exception, because google always return valid URL
    @SneakyThrows
    private User createFromPayload(GoogleIdToken.Payload payload) {
        return userRepository.save(new User(payload.getSubject(), payload.getEmail(), new URL((String) payload.get("picture")), List.of(), List.of(), List.of(), List.of(), UserRole.REGULAR, UserStatus.ACTIVE));
    }

    public List<String> filterBannedIds(User currentUser, List<String> ids) {
        if (currentUser.isAdmin()) {
            return ids;
        }

        List<String> bannedUsersIds = getAllBanned().stream().map(User::getId).filter(id -> !Objects.equals(id, currentUser.getId())).toList();
        List<String> filteredIds = new ArrayList<>(ids);
        filteredIds.removeAll(bannedUsersIds);
        return filteredIds;
    }

    private UserPublicInfoDTO createUserPublicInfoDTO(User currentUser, User user) {
        user.setFollowerIds(filterBannedIds(currentUser, user.getFollowerIds()));

        return new UserPublicInfoDTO(user, currentUser.getFollowingIds().contains(user.getId()));
    }

    public CurrentUserDTO getCurrentUser(User user) {
        return new CurrentUserDTO(user);
    }

    private User updateUser(User user, String newUsername, Optional<MultipartFile> newAvatar) {
        URL newAvatarURL = user.getAvatarURL();

        if (newAvatar.isPresent()) {
            try {
                newAvatarURL = imageRepository.uploadImage(newAvatar.get());
            } catch (FailedImageUploadException e) {
                logger.error(e.getMessage());
            }
        }

        user.setUsername(newUsername);
        user.setAvatarURL(newAvatarURL);
        userRepository.save(user);

        return user;
    }

    private boolean correctUsername(String newUsername) {
        return newUsername.matches("^(?=.{3,20}$)[a-z0-9_]+$");
    }

    public void processFollow(User currentUser, String followingName, Action action) throws NoSuchUserException {
        String followerId = currentUser.getId();
        User following = getByUsername(currentUser, followingName).orElseThrow(() -> new IncorrectNameException(followingName));
        String followingId = following.getId();

        DatabaseService.Action dbAction = action == Action.FOLLOW ? DatabaseService.Action.ADD : DatabaseService.Action.REMOVE;
        databaseService.modifySet("users", followingId, "followerIds", followerId, dbAction);
        databaseService.modifySet("users", followerId, "followingIds", followingId, dbAction);
    }

    public List<FollowerDTO> getUserFollowersByUsername(User currentUser, String username) throws NoSuchUserException {
        User user = getByUsername(currentUser, username).orElseThrow(() -> new NoSuchUserException(username));

        return user.getFollowerIds().stream().map(userId -> getById(currentUser, userId)).flatMap(Optional::stream).map(FollowerDTO::new).toList();
    }
}
