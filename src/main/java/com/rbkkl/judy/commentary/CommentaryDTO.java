package com.rbkkl.judy.commentary;

import com.rbkkl.judy.user.User;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.net.URL;
import java.time.Instant;

@Data
@AllArgsConstructor
public class CommentaryDTO {
    private final String username;
    private final URL avatarURL;
    private final String text;
    private final Instant time;

    public CommentaryDTO(User user, Commentary commentary) {
        this(user.getUsername(), user.getAvatarURL(), commentary.getText(), commentary.getTime());
    }
}