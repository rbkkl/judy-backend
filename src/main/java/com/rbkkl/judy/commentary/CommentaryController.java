package com.rbkkl.judy.commentary;

import com.rbkkl.judy.user.NoSuchUserException;
import com.rbkkl.judy.user.UserPrincipal;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/commentary")
@RequiredArgsConstructor
public class CommentaryController {
    private final CommentaryService commentaryService;

    @GetMapping("")
    public ResponseEntity<List<CommentaryWithPostIdDTO>> getUserCommentaries(
            @AuthenticationPrincipal UserPrincipal userPrincipal,
            @RequestParam(name = "username") String username) {
        try {
            return ResponseEntity.ok(commentaryService.getAllCommentariesByUsername(userPrincipal.getUser(), username));
        } catch (NoSuchUserException e) {
            // only if username from query is incorrect
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
    }
}
