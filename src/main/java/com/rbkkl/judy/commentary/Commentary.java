package com.rbkkl.judy.commentary;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Document(collection = "commentaries")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Commentary {
    @Transient
    public static final String SEQUENCE_NAME = "commentary_sequence";

    @Id
    private String id;
    private String userId;
    private String postId;
    private String text;
    private Instant time;
}
