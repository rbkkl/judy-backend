package com.rbkkl.judy.commentary;

import com.rbkkl.judy.post.Post;
import com.rbkkl.judy.sequence.SequenceService;
import com.rbkkl.judy.user.NoSuchUserException;
import com.rbkkl.judy.user.User;
import com.rbkkl.judy.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static com.rbkkl.judy.commentary.Commentary.SEQUENCE_NAME;

@Service
@RequiredArgsConstructor
public class CommentaryService {
    private final CommentaryRepository commentaryRepository;
    private final UserService userService;
    private final SequenceService sequenceService;

    public Commentary add(String userId, String postId, String text, Instant time) {
        var id = String.valueOf(sequenceService.generateNextSequence(SEQUENCE_NAME));
        Commentary newCommentary = new Commentary(id, userId, postId, text, time);
        return commentaryRepository.save(newCommentary);
    }

    public List<CommentaryWithPostIdDTO> getAllCommentariesByUsername(User currentUser, String username) {
        User user = userService.getByUsername(currentUser, username).orElseThrow(() -> new NoSuchUserException(username));

        return StreamSupport.stream(commentaryRepository.findAllById(user.getCommentariesIds()).spliterator(), false)
                .map(commentary -> new CommentaryWithPostIdDTO(user, commentary))
                .toList();
    }

    private Optional<CommentaryDTO> createCommentaryDTO(User currentUser, Commentary commentary) throws NoSuchUserException {
        return userService
                .getById(currentUser, commentary.getUserId()).stream()
                .map(user -> new CommentaryDTO(user, commentary)).findFirst();
    }

    public List<CommentaryDTO> getCommentariesDTO(User currentUser, Post post) {
        return StreamSupport.stream(commentaryRepository.findAllById(post.getCommentariesIds()).spliterator(), false)
                .map(commentary -> createCommentaryDTO(currentUser, commentary)).flatMap(Optional::stream)
                .toList();
    }
}
