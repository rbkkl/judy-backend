package com.rbkkl.judy.commentary;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentaryRepository extends MongoRepository<Commentary, String> {
    List<Commentary> findAllByUserId(String userId);
}
