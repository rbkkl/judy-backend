package com.rbkkl.judy.post;

public class NoSuchPostException extends RuntimeException {
    public NoSuchPostException(String postId) {
        super("No such post: " + postId);
    }
}