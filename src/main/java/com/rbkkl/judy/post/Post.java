package com.rbkkl.judy.post;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "posts")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Post {
    @Id
    private String id;
    private String authorId;
    private String text;
    private List<URL> imageURLs;
    private Instant time;
    private List<String> commentariesIds = new ArrayList<>();
    private List<String> likedIds = new ArrayList<>();
    private List<String> dislikedIds = new ArrayList<>();

    public void addCommentary(String commentaryId) {
        commentariesIds.add(commentaryId);
    }
}