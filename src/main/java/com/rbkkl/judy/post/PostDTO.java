package com.rbkkl.judy.post;

import com.rbkkl.judy.commentary.CommentaryDTO;
import com.rbkkl.judy.user.User;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.net.URL;
import java.time.Instant;
import java.util.List;
import java.util.Objects;


@Data
@AllArgsConstructor
public class PostDTO {
    private final String id;
    private final String text;
    private final List<URL> imageURLS;
    private final Instant time;
    private final int likesAmount;
    private final int dislikesAmount;
    private final List<CommentaryDTO> commentaries;
    private final String username;
    private final URL avatarURL;
    private final PostReaction reaction;

    public PostDTO(Post post, List<CommentaryDTO> commentariesDTO, User user, PostReaction reaction) {
        this(post.getId(),
                post.getText(),
                post.getImageURLs(),
                post.getTime(),
                post.getLikedIds().size(),
                post.getDislikedIds().size(),
                commentariesDTO,
                user.getUsername(),
                user.getAvatarURL(),
                reaction);
    }
}