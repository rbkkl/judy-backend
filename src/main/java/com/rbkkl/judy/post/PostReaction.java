package com.rbkkl.judy.post;

public enum PostReaction {
    NONE,
    DISLIKE,
    LIKE
}
