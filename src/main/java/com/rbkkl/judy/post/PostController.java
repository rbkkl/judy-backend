package com.rbkkl.judy.post;

import com.rbkkl.judy.image.FailedImageUploadException;
import com.rbkkl.judy.user.IncorrectNameException;
import com.rbkkl.judy.user.NoSuchUserException;
import com.rbkkl.judy.user.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/post")
public class PostController {
    @Autowired
    private PostService postService;

    @PostMapping("")
    @ResponseBody
    public ResponseEntity<PostDTO> publishPost(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                               @RequestParam(value = "text", required = false) String text,
                                               @RequestParam("images[]") MultipartFile[] images) {
        try {
            return ResponseEntity.ok(postService.publishPost(userPrincipal.getUser(), text, images));
        } catch (FailedImageUploadException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<PostDTO> getPost(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                           @PathVariable String id) {
        return postService.getPost(userPrincipal.getUser(), id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("")
    public ResponseEntity<List<PostDTO>> getPosts(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                                  @RequestParam(name = "user") Optional<String> username) {
        List<PostDTO> posts;
        if (username.isPresent()) {
            try {
                posts = postService.getUserPosts(userPrincipal.getUser(), username.get());
            } catch (NoSuchUserException e) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
            }
        } else {
            posts = postService.getAllPosts(userPrincipal.getUser());
        }
        posts = posts.stream().sorted(PostService::compareByCreationDateDesc).toList();
        return ResponseEntity.ok(posts);
    }

    @GetMapping("/feed")
    public ResponseEntity<List<PostDTO>> getFollowingUsersPosts(@AuthenticationPrincipal UserPrincipal userPrincipal) {
        List<PostDTO> followingUsersPosts = postService.getFollowingUsersPosts(userPrincipal.getUser());

        followingUsersPosts = followingUsersPosts.stream().sorted(PostService::compareByCreationDateDesc).toList();
        return ResponseEntity.ok(followingUsersPosts);
    }

    @PatchMapping("")
    public ResponseEntity<PostDTO> reaction(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                            @RequestParam(name = "act") String action,
                                            @RequestParam(name = "id") String id) {
        try {
            String userId = userPrincipal.getUser().getId();
            postService.processReaction(userId, id, PostReaction.valueOf(action));
            return getPost(userPrincipal, id);
        } catch (NoSuchUserException | IncorrectNameException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e.getMessage(), e);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Bad act parameter: " + action);
        }
    }

    @PostMapping("/{postId}/commentaries")
    public ResponseEntity<PostDTO> addCommentary(@AuthenticationPrincipal UserPrincipal userPrincipal,
                                                 @PathVariable(name = "postId") String postId,
                                                 @RequestParam(name = "text") String text) {
        try {
            var postDTO = postService.addCommentary(userPrincipal.getUser(), postId, text);
            return postDTO.map(ResponseEntity::ok).orElseThrow(() -> new NoSuchPostException(postId));
        } catch (NoSuchPostException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
