package com.rbkkl.judy.post;

import com.rbkkl.judy.sequence.SequenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.nio.ByteBuffer;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Base64;

@Service
@RequiredArgsConstructor
public class PostIdGenerator {
    // name for accessing an auto-incrementing post id in SequenceGenerator
    private static final String SEQUENCE_NAME = "UID";
    private static final int TOTAL_BITS = 64;
    // bits amount for time delta
    private static final int TIME_BITS = 41;
    // bits amount for shortened com.rbkkl.judy.user id
    private static final int USER_ID_BITS = 13;
    // bits amount for post id
    private static final int POST_ID_BITS = 10;
    // shorten com.rbkkl.judy.user id for this value
    private static final int USER_ID_CUT_SIZE = 19;
    // modulus for com.rbkkl.judy.user and post ids, so they fit in bits amount
    private static final int USER_ID_MOD = (int) (Math.pow(2, USER_ID_BITS));
    private static final int POST_ID_MOD = (int) (Math.pow(2, POST_ID_BITS));
    // custom epoch for time delta
    private final LocalDateTime epoch = LocalDateTime.of(2022, 1, 1, 0, 0);

    final private SequenceService sequenceService;

    public String generate(String userID, LocalDateTime timestamp) {
        // delta between custom epoch and current time in millis
        long delta = ChronoUnit.MILLIS.between(epoch, timestamp);

        long moddedUserId = Long.parseLong(userID.substring(0, USER_ID_CUT_SIZE)) % USER_ID_MOD;
        long moddedPostId = sequenceService.generateNextSequence(SEQUENCE_NAME) % POST_ID_MOD;

        // assemble uid
        long uid = delta << (TOTAL_BITS - TIME_BITS) |
                moddedUserId << (TOTAL_BITS - TIME_BITS - USER_ID_BITS) |
                moddedPostId;

        ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES).putLong(uid);
        // use base64 encoder to make nice string and change '/' to '+' so it don't ruin URLS
        var generatedId = Base64.getEncoder().encodeToString(buffer.array()).replace("/", "+");
        return generatedId;
    }
}
