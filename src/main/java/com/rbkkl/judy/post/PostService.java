package com.rbkkl.judy.post;

import com.rbkkl.judy.commentary.Commentary;
import com.rbkkl.judy.commentary.CommentaryDTO;
import com.rbkkl.judy.commentary.CommentaryService;
import com.rbkkl.judy.image.AzureImageRepository;
import com.rbkkl.judy.user.NoSuchUserException;
import com.rbkkl.judy.user.User;
import com.rbkkl.judy.user.UserRepository;
import com.rbkkl.judy.user.UserService;
import com.rbkkl.judy.util.DatabaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.net.URL;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

@Service
@RequiredArgsConstructor
public class PostService {
    private final CommentaryService commentaryService;
    private final PostRepository postRepository;
    private final PostIdGenerator generatorUUID;
    private final AzureImageRepository imageRepository;
    private final UserRepository userRepository;
    private final UserService userService;
    private final DatabaseService databaseService;

    private static final String COLLECTION_NAME = "posts";
    private static final String DISLIKED_SET_NAME = "dislikedIds";
    private static final String LIKED_SET_NAME = "likedIds";

    public PostDTO publishPost(User currentUser, String text, MultipartFile[] images) {
        List<URL> uploadedImagesURLS = Arrays.stream(images).map(imageRepository::uploadImage).toList();
        return createPostDTO(currentUser, createNewPost(currentUser, text, uploadedImagesURLS)).get(); // NOSONAR
    }

    private Optional<Post> getById(User currentUser, String postId) {
        return postRepository.findById(postId).stream()
                .filter(post -> userService.isUserShowable(currentUser, userRepository.findById(post.getAuthorId()).get()))
                .findFirst();
    }

    public List<PostDTO> getAllPosts(User currentUser) {
        var a = userService.getAll(currentUser);
        return userService.getAll(currentUser).stream()
                .flatMap(user -> StreamSupport.stream(
                        postRepository.findAllById(user.getPostsIds()).spliterator(), false))
                .map(post -> createPostDTO(currentUser, post)).flatMap(Optional::stream)
                .toList();
    }

    public Optional<PostDTO> getPost(User currentUser, String id) {
        return getById(currentUser, id).stream()
                .map(post -> createPostDTO(currentUser, post)).flatMap(Optional::stream)
                .findFirst();
    }

    private Optional<PostDTO> createPostDTO(User currentUser, Post post) {
        PostReaction reaction = PostReaction.NONE;
        if (post.getLikedIds().contains(currentUser.getId())) {
            reaction = PostReaction.LIKE;
        } else if (post.getDislikedIds().contains(currentUser.getId())) {
            reaction = PostReaction.DISLIKE;
        }
        PostReaction finalReaction = reaction;

        List<CommentaryDTO> commentariesDTO = commentaryService.getCommentariesDTO(currentUser, post);

        post.setLikedIds(userService.filterBannedIds(currentUser, post.getLikedIds()));
        post.setDislikedIds(userService.filterBannedIds(currentUser, post.getDislikedIds()));

        return userRepository.findById(post.getAuthorId()).stream()
                .map(user -> new PostDTO(post, commentariesDTO, user, finalReaction)).findFirst();
    }

    public List<PostDTO> getUserPosts(User currentUser, String username) throws NoSuchUserException {
        User user = userService.getByUsername(currentUser, username)
                .orElseThrow(() -> new NoSuchUserException(username));

        return user.getPostsIds().stream()
                .map(postRepository::findById).flatMap(Optional::stream)
                .map(post -> createPostDTO(currentUser, post)).flatMap(Optional::stream)
                .toList();
    }

    private Post createNewPost(User currentUser, String text, List<URL> imageURLs) {
        var authorId = currentUser.getId();

        var postId = generatorUUID.generate(authorId, LocalDateTime.now());
        var post = new Post(postId, authorId, text, imageURLs, Instant.now(), List.of(), List.of(), List.of());
        post = postRepository.save(post);

        currentUser.addPost(postId);
        userRepository.save(currentUser);

        return post;
    }

    public static int compareByCreationDateDesc(PostDTO postA, PostDTO postB) {
        return postB.getTime().compareTo(postA.getTime());
    }

    public List<PostDTO> getFollowingUsersPosts(User currentUser) {
        List<User> followingUsers = currentUser.getFollowingIds().stream()
                .map(followingId -> userService.getById(currentUser, followingId)).flatMap(Optional::stream).toList();

        return followingUsers.stream()
                .flatMap(followingUser -> StreamSupport
                        .stream(postRepository.findAllById(followingUser.getPostsIds()).spliterator(), false))
                .map(post -> createPostDTO(currentUser, post)).flatMap(Optional::stream)
                .toList();
    }

    public void processReaction(String userId, String postId, PostReaction reaction) throws NoSuchUserException {
        switch (reaction) {
            case NONE -> {
                databaseService.modifySet(COLLECTION_NAME, postId, DISLIKED_SET_NAME, userId, DatabaseService.Action.REMOVE);
                databaseService.modifySet(COLLECTION_NAME, postId, LIKED_SET_NAME, userId, DatabaseService.Action.REMOVE);
            }
            case LIKE -> {
                databaseService.modifySet(COLLECTION_NAME, postId, DISLIKED_SET_NAME, userId, DatabaseService.Action.REMOVE);
                databaseService.modifySet(COLLECTION_NAME, postId, LIKED_SET_NAME, userId, DatabaseService.Action.ADD);
            }
            case DISLIKE -> {
                databaseService.modifySet(COLLECTION_NAME, postId, DISLIKED_SET_NAME, userId, DatabaseService.Action.ADD);
                databaseService.modifySet(COLLECTION_NAME, postId, LIKED_SET_NAME, userId, DatabaseService.Action.REMOVE);
            }
        }
    }

    public Optional<PostDTO> addCommentary(User currentUser, String postId, String text) throws NoSuchPostException {
        Post post = postRepository.findById(postId)
                .orElseThrow(() -> new NoSuchPostException(postId));

        Commentary commentary = commentaryService.add(currentUser.getId(), postId, text, Instant.now());
        var commentaryId = commentary.getId();

        post.addCommentary(commentaryId);
        postRepository.save(post);
        currentUser.addCommentary(commentaryId);
        userRepository.save(currentUser);

        return createPostDTO(currentUser, post);
    }
}