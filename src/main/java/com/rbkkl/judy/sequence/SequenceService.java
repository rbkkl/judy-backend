package com.rbkkl.judy.sequence;

import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SequenceService {
    // update rule (actually incrementing value at key 'sequence')
    Update update = new Update().inc("sequence", 1);

    private final MongoOperations mongoOperations;

    public long generateNextSequence(final String sequenceName) {
        DatabaseSequence counter = mongoOperations.findAndModify(
                // only increment ids that bound to specific sequenceName
                new Query(Criteria.where("id").is(sequenceName)),
                update,
                FindAndModifyOptions.options().returnNew(true).upsert(true),
                DatabaseSequence.class
        );
        return counter != null ? counter.getSequence() : 1;
    }
}
