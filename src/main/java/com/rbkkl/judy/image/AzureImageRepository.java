package com.rbkkl.judy.image;

import com.azure.core.util.BinaryData;
import com.azure.storage.blob.BlobClient;
import com.azure.storage.blob.BlobContainerClient;
import com.azure.storage.blob.BlobServiceClientBuilder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.net.URL;
import java.util.Objects;
import java.util.UUID;

@Repository
public class AzureImageRepository implements ImageRepository {
    private static final String CONTAINER_NAME = "images";

    public final BlobContainerClient containerClient;

    public AzureImageRepository(@Value("${services.azureBlobConnectionString}") String connectionString) {
        containerClient = new BlobServiceClientBuilder()
                .connectionString(connectionString)
                .buildClient()
                .getBlobContainerClient(CONTAINER_NAME);
    }


    @Override
    public URL uploadImage(MultipartFile image) {
        if (!Objects.requireNonNull(image.getContentType()).contains("image/")) {
            throw new FailedImageUploadException("Wrong mimetype");
        }
        BlobClient blobClient = containerClient.getBlobClient(String.valueOf(UUID.randomUUID()));
        try {
            blobClient.upload(BinaryData.fromBytes(image.getBytes()));
            return new URL(blobClient.getBlobUrl());
        } catch (Exception e) {
            throw new FailedImageUploadException(e);
        }
    }
}
