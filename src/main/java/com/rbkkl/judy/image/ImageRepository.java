package com.rbkkl.judy.image;

import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import java.net.URL;

@Repository
public interface ImageRepository {
    URL uploadImage(MultipartFile image) throws FailedImageUploadException;
}
