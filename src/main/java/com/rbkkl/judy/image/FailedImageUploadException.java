package com.rbkkl.judy.image;

public class FailedImageUploadException extends RuntimeException {
    public FailedImageUploadException(String message) {
        super(message);
    }

    public FailedImageUploadException(Throwable cause) {
        super(cause);
    }
}
