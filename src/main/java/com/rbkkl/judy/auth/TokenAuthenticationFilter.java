package com.rbkkl.judy.auth;

import com.rbkkl.judy.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Component
public class TokenAuthenticationFilter extends OncePerRequestFilter {
    private static final String TOKEN_IDENTIFIER = "Bearer";
    @Autowired
    private AuthenticationTokenService authenticationTokenService;
    @Autowired
    private UserService userService;

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain
    ) throws ServletException, IOException {
        getTokenFromRequest(request)
                .stream() // creates a stream of 1 element for processing a chain of methods that return Optionals
                .map(authenticationTokenService::getTokenPayload)
                .flatMap(Optional::stream) // get T instead of Optional<T>
                .map(userService::getPrincipal)
                .forEach(userPrincipal -> authenticateUser(request, userPrincipal));

        filterChain.doFilter(request, response);
    }

    private Optional<String> getTokenFromRequest(HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader("Authorization")).stream()
                .filter(token -> token.startsWith(TOKEN_IDENTIFIER))
                .map(token -> token.substring(TOKEN_IDENTIFIER.length()).trim())
                .findFirst();
    }

    private void authenticateUser(HttpServletRequest request, UserDetails userDetails) {
        var authentication = new UsernamePasswordAuthenticationToken(
                userDetails, null, userDetails.getAuthorities());
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
