package com.rbkkl.judy.auth;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Optional;
import java.util.logging.Level;

@Log
@Service
public class AuthenticationTokenService {
    private final GoogleIdTokenVerifier verifier;

    @Autowired
    public AuthenticationTokenService(@Value("${services.googleOAuthClientId}") String clientID) {
        verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), new GsonFactory())
                .setAudience(Collections.singletonList(clientID))
                .build();
    }

    public Optional<GoogleIdToken.Payload> getTokenPayload(String token) {
        try {
            return Optional.of(verifier.verify(token).getPayload());
        } catch (GeneralSecurityException | IOException | NullPointerException e) {
            log.log(Level.WARNING, "Invalid token");
            return Optional.empty();
        }
    }
}
