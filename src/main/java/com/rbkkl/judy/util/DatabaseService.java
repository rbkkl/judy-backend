package com.rbkkl.judy.util;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class DatabaseService {
    private final MongoTemplate mongoTemplate;

    public enum Action {
        ADD,
        REMOVE,
    }

    public void modifySet(String collectionName, String id, String setName, Object value, Action action) {
        mongoTemplate.updateFirst(
                Query.query(Criteria.where("_id").is(id)),
                action == Action.ADD ? new Update().addToSet(setName, value)
                        : new Update().pull(setName, value),
                collectionName
        );
    }
}
