FROM maven:3.8.4-openjdk-17-slim as builder
ENV HOME=/usr/app
WORKDIR $HOME
COPY pom.xml $HOME
RUN mvn verify --fail-never
COPY src $HOME/src
RUN mvn package -DskipTests

FROM openjdk:17.0.2-jdk-slim
ENV HOME=/usr/app
WORKDIR $HOME
COPY --from=builder /usr/app/target/judy-*.jar $HOME/judy.jar
ENV PORT 8080
EXPOSE $PORT
CMD [ "java", "-jar", "/usr/app/judy.jar" ]
